import './App.scss'

import * as React from 'react';
import * as api from './api'
import { MemberId, Member, ExpenseData, Statements } from './types/CommonTypes'

import { MemberList } from './components/MemberList'
import { MemberDetails } from './components/MemberDetails';

interface AppState {
  members: Member[]
  selectedMember: Member | null
  memberExpenses: Map<Member, ExpenseData>
  memberStatements: Map<Member, Statements>
}

class App extends React.Component<any, AppState> {
  
  public state: AppState

  constructor (props: any) {
    super(props)
    this.state = {
      members: [],
      selectedMember: null,
      memberExpenses: new Map<Member, ExpenseData>(),
      memberStatements: new Map<Member, Statements>()
    }
    
    this.fetchMemberData()
  }

  public render() {
    
    const setSelectedMember = (member: Member) => this.setSelectedMember(member)
    const { selectedMember, memberExpenses, memberStatements } = this.state 

    let expensesForMember
    let statementsForMember
    
    if ( selectedMember ) {
      expensesForMember = memberExpenses.get(selectedMember)
      statementsForMember = memberStatements.get(selectedMember)
    }

    return (
      <div className="app">
        <MemberList 
          members={ this.state.members }
          selectedMember= { this.state.selectedMember }
          selectMember={ setSelectedMember }
        />

        <MemberDetails 
          member={ selectedMember }
          expenses={ expensesForMember }
          statements={ statementsForMember }
        />

      </div>
    );
  }

  // Action to take when a member is selected from the list
  public setSelectedMember (member: Member): void {
    // Set the current member
    this.setState({
      selectedMember: member
    })
    
    // If we've not got a cached version, make a request
    if (!this.state.memberStatements.get(member)) {
      this.fetchMemberStatements(member)
    }

    // Same for expenses
    if (!this.state.memberExpenses.get(member)) {
      this.fetchMemberExpenses(member)
    }
  }
  
  // Fetches the list of members
  private fetchMemberData (): void {
    api.fetchMemberData().then((members => {
      
      // The api takes care of processing the data to get an actual usable list
      this.setState({ members })
      
      // Select the first item in the list by default
      this.setSelectedMember(members[0])
    }))
  }

  // Fetches the statements for the relevant member
  private fetchMemberStatements (member: Member): void {
    api.fetchStatements(member.id).then((statements) => {
      // Once the statements are in, cache them, and update the state
      const m = this.state.memberStatements
      m.set(member, statements)
      this.setState({memberStatements: m})
    })
  }

  // As above
  private fetchMemberExpenses (member: Member): void {
    api.fetchExpenseData(member.id).then((expenseData) => {
      // Update the cache as before
      const m = this.state.memberExpenses
      m.set(member, expenseData)
      this.setState({memberExpenses: m})
    })
  }
  
}

export default App;
