import Axios from 'axios'

import { Member, MemberId, Statements, ExpenseData, ExpenseDataPoint } from './types/CommonTypes'

const apiKey = '8nxiD771T6MQtkLhd1YBsgcmSskeYhfouvan6R1v'
const baseURL = 'https://api.propublica.org/congress/v1/'
const congress = '115'
const chamber = 'house'

const connect = Axios.create({
  baseURL,
  method: 'GET',
  headers: {'X-API-Key': apiKey}
})

export const fetchMemberData = (): Promise<any> => {
  // We're just going to assume for now that a) this works and b) the response doesn't change
  return connect.get(`${congress}/${chamber}/members.json`, {
    transformResponse: (data, headers) => {
      const members = JSON.parse(data).results[0].members

      const processedMembers: Member[] = members

      // Filter out anyone not representing California
      .filter((raw: any) => {
        return raw.state === 'CA'
      })
      
      // Convert into a form we can use
      .map((raw: any) => {
        return {
          id: raw.id,
          title: raw.short_title,
          firstName: raw.first_name,
          middleName: raw.middle_name,
          lastName: raw.last_name,
          party: raw.party,
          totalVotes: raw.total_votes,
          missedVotes: raw.missed_votes,
          partisanship: raw.votes_with_party_pct,
          youtube: raw.youtube_account
        }
      }) 
      return processedMembers
    }
  }).then((response) => {
    return response.data
  })
}

export const fetchStatements = (id: MemberId): Promise<Statements> => {
  // As above, we're just going to keep our fingers crossed
  return connect.get(`members/${id}/statements/${congress}.json`, {
    transformResponse: (data, headers) => {
      const statements = JSON.parse(data).results
      const processedStatements: Statements = statements.map((raw: any) => {
        return {
          date: raw.date,
          type: raw.statement_type,
          title: raw.title,
          url: raw.url
        }
      })
      return processedStatements
    }
  }).then((response) => {
    return response.data
  })
}

const yearAndQuarterToDate = (year: number, quarter: number) => {
  // This gives the first day of the month of the last month of the quarter, which isn't great, but will do for now.
  return new Date(year, (quarter - 1) * 3 + 2)  
}

// Slightly more to do here
export const fetchExpenseData = (id: MemberId): Promise<ExpenseData> => {
  return connect.get(`members/${id}/office_expenses/category/travel.json`, {
    transformResponse: (data, headers) => {
      const datums = JSON.parse(data).results.reverse()
      const processedDatums: ExpenseDataPoint[] = datums.map((raw: any) => {
        return {
          date: yearAndQuarterToDate(raw.year, raw.quarter),
          value: raw.amount
        }
      })

      // Summarise the data to make graphing easier
      // This should be a separate function really, but it's getting late
      let total = 0
      let min = Number.POSITIVE_INFINITY
      let max = Number.NEGATIVE_INFINITY

      processedDatums.forEach((d) => {
        if (d.value < min) {
          min = d.value
        }
        if (d.value > max) {
          max = d.value
        }
        total += d.value
      })

      return {
        startDate: processedDatums[0].date,
        endDate: processedDatums[processedDatums.length - 1].date,
        dataPoints: processedDatums,
        total,
        min,
        max           
      }
      
    }
  }).then((response) => {
    return response.data
  })
}