// Dropping into regular JS for this because the TS for defining the properties as a type was doing my head in. 
// Something to revisit.

import * as React from 'react'
import { select } from 'd3'

export class Axis extends React.Component {

  constructor(props) {
    super(props)
    this.el = null
  }

  componentDidMount () {
    this.renderAxis()
  }

  componentDidUpdate () {
    this.renderAxis()
  }

  renderAxis () {
    const { axis } = this.props
    select(this.el).call(axis)
  }

  render () {
    return (
      <g ref={ (el) => { this.el = el } }></g>
    )
  }

}