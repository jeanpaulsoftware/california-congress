import * as React from 'react'
import { line, scaleLinear, scaleTime, axisLeft, axisBottom, format } from 'd3'

import { Axis } from './Axis'

import { ExpenseData, ExpenseDataPoint } from '../types/CommonTypes'

interface LineChartProps {
  data: ExpenseData
}

interface ChartState {
  width: number
  height: number
}

// Very standard d3 line chart- take the data and plot it. 
export class LineChart extends React.Component<LineChartProps> {

  public state: ChartState

  public reference: HTMLDivElement

  public dimensionUpdateListener: () => void

  constructor (props: LineChartProps) {
    super(props)
    this.state = {
      width: 950,
      height: 350
    }
  }

  public render () {
    
    const { width, height } = this.state
    const { data } = this.props
    const { min, max, total, dataPoints } = data
    const xMargin = 30
    const yMargin = 30

    const xScale = scaleTime()
      .range([xMargin, width - xMargin / 2])
      .domain([data.startDate, data.endDate])

    const yScale = scaleLinear()
      .range([height - yMargin, 0])
      .domain([min, max])

    const lineDef = line<ExpenseDataPoint>()
      .x((d) => xScale(d.date))
      .y((d) => yScale(d.value))

    const pathString = lineDef(data.dataPoints)

    const bottomAxis = axisBottom(xScale)
    const leftAxis = axisLeft(yScale)
      .ticks(10)
      .tickFormat(format('.2s'))
      .tickSize(-width + xMargin * 1.5 )
    
    return (
      <div className="line-chart" ref={(element) => { this.reference = element! }}>
          <svg width={width} height={height}>
            <g transform="translate(0, 10)">
              <g transform={ `translate(${xMargin}, 0)` } className="leftAxis">
                <Axis axis={ leftAxis } />
              </g>
              <g transform={ `translate(0, ${height - yMargin})` }>
                <Axis axis={ bottomAxis } />
              </g>
              <path 
                d={ pathString! }
                fill='none'
                stroke='#00AA00'
                strokeWidth='2' 
              />
            </g>
          </svg>
      </div>
    )
  }

  // Some extra work is required to redraw the SVG when resizing
  public updateDimensions() {
    const {width, height} = this.reference.getBoundingClientRect()
    this.setState({width, height})
  }

  public componentDidMount () {
    this.dimensionUpdateListener = this.updateDimensions.bind(this)
    window.addEventListener('resize', this.dimensionUpdateListener)
    this.updateDimensions()
  }

  public componentWillUnmount() {
    window.removeEventListener('resize', this.dimensionUpdateListener)
  }

}