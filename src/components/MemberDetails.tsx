import './MemberDetails.scss'
import * as React from 'react'
import { concatenateMemberName } from '../util'
import { Member, ExpenseData, Statements, Statement } from 'src/types/CommonTypes';
import { LineChart } from './LineChart'
import { VotingRecord } from './VotingRecord'

// Main view for showing details about a member of congress. There are three sections, each displaying one of the 
// items in the use cases.

// Each pane will load independently if the data required for it is available.

// The different parts could do with being broken out into their own files and classes better - in particular the
// three panels as defined in this file have near-identical loading behaviour that could be abstracted nicely. However,
// this happens when you're in a rush.

export interface MemberDetailsProps {
  member: Member | null
  expenses: ExpenseData | undefined
  statements: Statements | undefined
}

export class MemberDetails extends React.Component<MemberDetailsProps> {

  public render () {

    const {member, expenses, statements} = this.props

    // Stiches together the different elements using the functions below
    return (
      <div className="member-details">
        { this.getHeader() }
        <div className="row">{ this.getExpenses() }</div>
        <div className="row">
          { this.getVotingBehaviour() }
          { this.getStatements() }
        </div>
      </div>
    )
  }

  // Gets the header part of the display
  private getHeader () {

    const { member } = this.props

    if (!member) {
      return ( 
        <div className="header">
          <div className="heading">Loading...</div>
          <div>&nbsp;</div>
        </div>
      )
    }
    return (
      <div className="header">
        <div className={'heading ' + member.party}>{ concatenateMemberName(member) } ({member.party})</div>
        <div>{
          member.youtube ? <a href={'http://youtube.com/' + member.youtube} target="_new">{`${member.title} ${member.lastName}'s YouTube channel`}</a> : 'This member has no presence on YouTube'
        }</div>
      </div>
    )
  }

  // Gets the expenses graph and details, should one be available
  private getExpenses () {
    const { expenses } = this.props
    if (!expenses) {
      return (
        <div className="cell expenses">
          <div className="subheading">Loading member travel expenses...</div>
        </div>
      )
    }
    return (
      <div className="cell expenses">
        <div className="subheading">
          Total travel expenses {expenses.startDate.toLocaleString('en-US', {month: 'long', year: 'numeric'})} to&nbsp;
          {expenses.endDate.toLocaleString('en-US', {month: 'long', year: 'numeric'})}:&nbsp;
          <span className="total">${expenses.total.toFixed(2)}</span>
        </div>
        <LineChart data={ expenses }/>
      </div>
    )
  }

  // Gets the panel showing voting behaviour
  private getVotingBehaviour() {
    const { member } = this.props
    if (!member) {
      return (
        <div className="cell voting">
          <div className="subheading">Loading member voting record...</div>
        </div>
      )
    }
    return (
      <div className="cell voting">
        <div className="subheading">Voting record for { concatenateMemberName(member) } </div>
        <VotingRecord member={member} />
      </div>
    )
    
  }

  // Gets the list of statements the member has made
  private getStatements () {
    const { statements, member } = this.props
    if (!statements || !member) {
      return (
        <div className="cell statements">
          <div className="subheading">Loading recent member statements...</div>   
        </div>
      )
    }

    const listContent = statements.map((statement: Statement, index) => {
      return (
        <div className="statement" key={index}>
          <div className="info">
            <span className="type">{statement.type}</span>, <span className="date">{statement.date}</span> 
          </div>
          <div className="link">
            <a href={statement.url} target="_new">{statement.title}</a>
          </div>
        </div>
      )
    })

    return (
      <div className="cell statements">
        <div className="subheading">Recent statements by { concatenateMemberName(member) }</div>
        <div className="list">
          {listContent}
        </div>
      </div>
    )
  }




}