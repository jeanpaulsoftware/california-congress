import * as React from 'react';
import './MemberList.scss'

import { Member } from '../types/CommonTypes'
import { concatenateMemberName } from '../util'

// Simple component for displaying the name of the member

interface MemberListItemProps {
  member: Member
  onClick: (m: Member) => void
  selected: boolean
}

const MemberListItem = (props: MemberListItemProps) => {

  const { member, onClick, selected } = props
  const { title, firstName, middleName, lastName, party } = member
  const select = () => { onClick(member) }

  return (
    <div className={'member-list-item ' + party + ' ' + (selected ? 'selected' : '')} onClick={ select } >
      <span className="member-name">{ concatenateMemberName(member) }</span>
      <span className={'party'}> ({ party })</span>
    </div>
  )
}


// Main class for the list

export interface MemberListProps {
  members: Member[]
  selectedMember: Member | null
  selectMember(member: Member): void
}

export interface MemberListState {
  searchText: string
}

export class MemberList extends React.Component<MemberListProps, MemberListState> {

  public state: MemberListState

  constructor(props: MemberListProps) {
    super(props)
    this.state = {
      searchText: ''
    }
  }

  public render() {
    
    const { members, selectedMember, selectMember } = this.props

    // Simple function to update search text
    const updateSearchText = (e: React.ChangeEvent<HTMLInputElement>) => this.setState({searchText: e.target.value})
    
    // When a member gets clicked on, use the callback
    const onMemberClick = (member: Member): void => selectMember(member)

    // Apply any search term the user has put in 
    const memberItems = members
      .filter((member: Member) => {
        return (
          member.firstName.toLowerCase().indexOf(this.state.searchText.toLowerCase()) > -1 || 
          member.lastName.toLowerCase().indexOf(this.state.searchText.toLowerCase()) > -1
        )      
      })
      .map((member: Member, index) => {
        return <MemberListItem 
          key={ member.id }
          member={ member }
          onClick={ onMemberClick }
          selected={ member === selectedMember }
        />
      })

    return (
      <div className="member-list">
        
        <div className="header">
          <div className="heading">Congress Members for California</div>
          <div className="info">Select a member from the list below to view details. Use the controls below to search for a member.</div>
          <div className="filter">
            <div className="search">
              <span>Search by name: </span>
              <input type="text" value={this.state.searchText} onChange={ updateSearchText }/>
            </div>
            <div className="select-party" />
          </div>
        </div>

        <div className="list">
          { memberItems.length === 0 ? <span>Loading...</span> : memberItems }
        </div>
      </div>
    )
  }

}

