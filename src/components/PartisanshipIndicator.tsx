import * as React from 'react'
import { Member } from '../types/CommonTypes'

export interface IndicatorProps {
  member: Member
}

// Please excuse the rough nature of the way this looks, it's a proof of concept.
// Visualises how partisan a particular member is based on how often they vote along party lines

// Ideally this should also show the average, but I don't think I'll get time.

export class PartisanshipIndicator extends React.Component<IndicatorProps> {

  public render () {

    const { member } = this.props
    const { partisanship, party } = member

    const x1 = 50
    const x2 = 425
    const width = x2 - x1
    
    let normalisedPartisanship = partisanship / 100

    // Decide how far along to put the indicator

    if (party === 'D') {
      normalisedPartisanship = 1 - normalisedPartisanship
    }

    const tagPosition = x1 + width * normalisedPartisanship

    return (
      
        <svg 
          className="partisanship"
          width="476"
          height="40"
        >
          <defs> 
            <linearGradient id="spectrum" x1="0%" y1="0%" x2="100%" y2="0%" > 
              <stop offset="0%" stopColor="rgba(0,0,170,0.5)" />
              <stop offset="100%" stopColor="rgba(170,0,0,0.5)" />
            </linearGradient> 
          </defs>

          <g transform="translate(0, 20)">
            <text className="D">Dem.</text>
            <text 
              className="R"
              transform="translate(435, 0)"
            >Rep.</text>

            <line 
              className="background"  
              strokeWidth="10"
              strokeLinecap="round" 
              stroke="url(#spectrum)"
              x1={x1} y1="-5"
              x2={x2} y2="-5.0001"
            />

            <line 
              className="tag"
              strokeWidth="10"
              stroke="black"
              x1={tagPosition}
              x2={tagPosition}
              y1={-12}
              y2={4}
              strokeLinecap="round"
              
            />
    
          </g>
        
        </svg>
      
    )
  }

}