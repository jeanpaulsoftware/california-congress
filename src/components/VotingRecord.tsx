import * as React from 'react'
import { Member } from '../types/CommonTypes'
import { PartisanshipIndicator } from './PartisanshipIndicator'
 
export interface VotingRecordProps {
  member: Member
}

export class VotingRecord extends React.Component<VotingRecordProps> {

  public render () {

    const { member } = this.props
    const { partisanship, missedVotes, totalVotes, party } = member
    
    return (
      <div className="record">
        <div className={'party ' + party}>Party: { party === 'D' ? 'Democratic' : 'Republican' }</div>
        <div>{`Voted along party lines ${member.partisanship}% of the time`}</div>
        <PartisanshipIndicator member= {member} />
        <div>{`Was absent for ${member.missedVotes} of ${member.totalVotes} votes`}</div>
      </div>
    )
  }

}