/* 
This project was created using the typescript variant of 'create-react-app', which I played around with 
over the weekend - https://facebook.github.io/create-react-app/

There's some quirks, such as importing styles directly from JavaScript, but it's very quick to set up for
projects like this.
*/

import * as React from 'react'
import * as ReactDOM from 'react-dom'
import App from './App'
import './Index.scss'

ReactDOM.render(
  <App />,
  document.getElementById('root') as HTMLElement
)