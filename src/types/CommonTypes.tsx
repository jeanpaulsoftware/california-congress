export type MemberId = string

export enum Party {
  Democrat = 'D',
  Republican = 'R'
}

export interface Member {
  id: MemberId
  title: string
  firstName: string
  middleName?: string
  lastName: string
  party: Party
  totalVotes: number
  missedVotes: number
  partisanship: number
  youtube?: string // I mean sure, why the hell not
}

export interface Statement {
  date: string
  type: string
  title: string
  url: string
}

export type Statements = Statement[]

export interface ExpenseDataPoint {
  date: Date
  value: number
}

export interface ExpenseData {
  startDate: Date
  endDate: Date
  min: number
  max: number
  dataPoints: ExpenseDataPoint[]
  total: number
}




