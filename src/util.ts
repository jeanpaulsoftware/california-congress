import { Member } from './types/CommonTypes';

export const concatenateMemberName = (member: Member) => {
  const { title, firstName, middleName, lastName } = member
  let out = title + ' ' + firstName
  if (middleName) {
    out += ' ' + middleName
  }
  return out + ' ' + lastName
}